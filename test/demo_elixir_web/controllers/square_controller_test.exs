defmodule DemoElixirWeb.SquareControllerTest do
  @moduledoc """
  Should get side length of square as query parameter
  """
  use DemoElixirWeb.ConnCase

  test "with integer", %{conn: conn} do
    conn = get conn, "/area/square?length=10"
    assert json_response(conn, 200) == %{
      "area" => 100,
      "shape" => "square",
      "params" => %{
        "length" => 10
      }
    }
  end

  test "with float", %{conn: conn} do
    conn = get conn, "/area/square?length=1.2"
    assert json_response(conn, 200) == %{
      "area" => 1.44,
      "shape" => "square",
      "params" => %{
        "length" => 1.2
      }
    }
  end

  test "with string", %{conn: conn} do
    conn = get conn, "/area/square?length=\"1.3\""
    assert json_response(conn, 400)
  end

  test "with negative value", %{conn: conn} do
    conn = get conn, "/area/square?length=-4"
    assert json_response(conn, 400)
  end

  test "missing values", %{conn: conn} do
    conn = get conn, "/area/square"
    assert json_response(conn, 400)
  end
end
