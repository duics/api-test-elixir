defmodule DemoElixirWeb.CircleControllerTest do
  @moduledoc """
  Should get radius of circle as path
  """
  use DemoElixirWeb.ConnCase

  test "with integers", %{conn: conn} do
    conn = get conn, "/area/circle/1"
    response = json_response(conn, 200)
    %{"area" => area} = response

    assert Map.drop(response, ["area"]) == %{
      "shape" => "circle",
      "params" => %{
        "radius" => 1
      }
    }
    assert Float.round(area, 2) == 3.14
  end

  test "with floats", %{conn: conn} do
    conn = get conn, "/area/circle/1.43"
    response = json_response(conn, 200)
    %{"area" => area} = response

    assert Map.drop(response, ["area"]) == %{
      "shape" => "circle",
      "params" => %{
        "radius" => 1.43
      }
    }
    assert Float.round(area, 2) == 6.42
  end

  test "missing values", %{conn: conn} do
    assert_raise Phoenix.Router.NoRouteError, fn ->
      get conn, "/area/circle/"
    end
  end

  test "zero value", %{conn: conn} do
    conn = get conn, "/area/circle/0"
    response = json_response(conn, 200)
    %{"area" => area} = response

    assert area == 0
  end

  test "string values", %{conn: conn} do
    conn = get conn, "/area/circle/hello"
    assert json_response(conn, 400)
  end

  test "negative values", %{conn: conn} do
    conn = get conn, "/area/circle/-123"
    assert json_response(conn, 400)
  end
end
