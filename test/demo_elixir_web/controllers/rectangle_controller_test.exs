defmodule DemoElixirWeb.RectangleControllerTest do
  @moduledoc """
  Should get length of sides as body parameter
  """
  use DemoElixirWeb.ConnCase

  test "with integers", %{conn: conn} do
    conn = post conn, "/area/rectangle", %{height: 10, width: 2}
    assert json_response(conn, 200) == %{
      "area" => 20,
      "shape" => "rectangle",
      "params" => %{
        "height" => 10,
        "width" => 2
      }
    }
  end

  test "with floats", %{conn: conn} do
    conn = post conn, "/area/rectangle", %{height: 10, width: 2}
    assert json_response(conn, 200) == %{
      "area" => 20,
      "shape" => "rectangle",
      "params" => %{
        "height" => 10,
        "width" => 2
      }
    }
  end

  test "floats with integers", %{conn: conn} do
    conn = post conn, "/area/rectangle", %{height: 1, width: 0.9999999999999999999999}
    response = json_response(conn, 200)
    assert response == %{
      "shape" => "rectangle",
      "area" => 1.0,
      "params" => %{
        "height" => 1,
        "width" => 0.9999999999999999999999
      }
    }
  end

  test "float responses", %{conn: conn} do
    conn = post conn, "/area/rectangle", %{height: 0.333333333333, width: 0.9999999999999999999999}
    response = json_response(conn, 200)
    assert Float.round(response["area"], 3) == 0.333
  end

  test "missing values", %{conn: conn} do
    conn = post conn, "/area/rectangle", %{height: 2.5}
    assert json_response(conn, 400)
  end

  test "string values", %{conn: conn} do
    conn = post conn, "/area/rectangle", %{height: "hello", width: 2}
    assert json_response(conn, 400)
  end

  test "negative values", %{conn: conn} do
    conn = post conn, "/area/rectangle", %{height: -1, width: -5.0}
    assert json_response(conn, 400)
  end
end
