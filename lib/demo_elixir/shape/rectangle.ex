defmodule DemoElixir.Shape.Rectangle do
  @moduledoc """
  This module contains functions for calculating the area for rectangles
  """

  def get_area(_) do
    nil
  end
end
