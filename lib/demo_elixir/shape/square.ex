defmodule DemoElixir.Shape.Square do
  @moduledoc """
  This module contains functions for calculating the area for squares
  """

  def get_area(_) do
    nil
  end
end
