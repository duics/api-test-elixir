defmodule DemoElixir.Shape.Circle do
  @moduledoc """
  This module contains functions for calculating the area for circles
  """

  def get_area(_) do
    nil
  end
end
