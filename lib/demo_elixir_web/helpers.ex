defmodule DemoElixirWeb.Helpers do

  @doc """
  Normally you'd just "let things crash" in elixir, so
  try/catch blocks in this instance are somewhat an anti-pattern
  """
  # If function is number, just return it
  def parse_number(number) when is_number(number), do: {:ok, number}
  # Otherwise parse the string
  def parse_number(string) when is_binary(string) do
    # If string contains a comma, it's probably a float
    # There are more elegant approaches than this
    try do
      number =
        if string =~ "." do
          String.to_float(string)
        else
          String.to_integer(string)
        end
      # Return a tuple that indicates that everything went as expected
      # Quite similar to tuples in scala
      {:ok, number}
    rescue
      err -> {:error, err}
    end
  end

  def zero_or_larger(num), do: num >= 0
end
