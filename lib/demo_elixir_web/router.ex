defmodule DemoElixirWeb.Router do
  use DemoElixirWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/area", DemoElixirWeb do
    pipe_through :api

    get "/square", SquareController, :index
    post "/rectangle", RectangleController, :index
    get "/circle/:radius", CircleController, :show
  end
end
