defmodule DemoElixirWeb.CircleController do
  @moduledoc """
  Should get radius of circle as path
  """
  use DemoElixirWeb, :controller
  alias DemoElixirWeb.Helpers

  def show(conn, %{"radius" => radius_string}) do
    # With is comparable to an if-clause with multiple conditions
    with {:ok, radius} <- Helpers.parse_number(radius_string),
         true <- Helpers.zero_or_larger(radius) do

      # Form a json body
      response = %{
        area: :math.pi() * radius * radius,
        shape: "circle",
        params: %{radius: radius},
      }

      json(conn, response)
    else
      # If conditions didn't match, the control flow is catched by
      # pattern matching the error or malformed value.
      # '_' is used to catch everything
      _ ->
        conn
        |> put_status(400)
        |> json("Malformed response")
    end
  end
  def index(conn, _) do
    conn
    |> put_status(400)
    |> json("Missing parameters")
  end
end
