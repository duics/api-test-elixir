defmodule DemoElixirWeb.RectangleController do
  @moduledoc """
  Should get width of sides as body parameter
  """
  use DemoElixirWeb, :controller
  alias DemoElixirWeb.Helpers

  @doc """
  Receive connection and destruct width parameter from params
  """
  def index(conn, %{"height" => height_string, "width" => width_string}) do

    # With is comparable to an if-clause with multiple conditions
    with {:ok, width} <- Helpers.parse_number(width_string),
         {:ok, height} <- Helpers.parse_number(height_string),
         true <- Helpers.zero_or_larger(width),
         true <- Helpers.zero_or_larger(height) do

      # Form a json body
      response = %{
        area: width * height,
        shape: "rectangle",
        params: %{width: width, height: height},
      }

      json(conn, response)
    else
      # If conditions didn't match, the control flow is catched by
      # pattern matching the error or malformed value.
      # '_' is used to catch everything
      _reason ->
        conn
        |> put_status(400)
        |> json("Malformed response")
    end
  end
  def index(conn, _) do
    conn
    |> put_status(400)
    |> json("Missing parameters")
  end
end
