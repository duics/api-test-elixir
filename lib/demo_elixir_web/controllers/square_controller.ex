defmodule DemoElixirWeb.SquareController do
  @moduledoc """
  Should get side length of square as query parameter
  """
  use DemoElixirWeb, :controller
  alias DemoElixirWeb.Helpers

  @doc """
  Receive connection and destruct length parameter from params
  """
  def index(conn, %{"length" => length_string}) do

    # With is comparable to an if-clause with multiple conditions
    with {:ok, length} <- Helpers.parse_number(length_string),
         true <- Helpers.zero_or_larger(length) do

      # Form a json body
      response = %{
        area: length * length,
        shape: "square",
        params: %{length: length},
      }

      json(conn, response)
    else
      # If conditions didn't match, the control flow is catched by
      # pattern matching the error or malformed value.
      # '_' is used to catch everything
      _ ->
        conn
        |> put_status(400)
        |> json("Malformed response")
    end
  end
  def index(conn, _) do
    conn
    |> put_status(400)
    |> json("Missing parameters")
  end
end
